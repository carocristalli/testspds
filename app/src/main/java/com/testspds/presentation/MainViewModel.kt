package com.testspds.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.testspds.data.UseCaseResult
import com.testspds.data.repository.WPRepository
import com.testspds.domain.Post
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel(private val wpRepository: WPRepository) : ViewModel() {

    val showError = MutableLiveData<String>()
    val postList = MutableLiveData<List<Post>>()

    fun loadInformation() {
        viewModelScope.launch {
            val result =
                withContext(Dispatchers.IO) { wpRepository.getInformation() }
            when (result) {
                is UseCaseResult.Success -> postList.value = result.data
                is UseCaseResult.Error -> showError.value = result.exception.message
            }
        }
    }

}
