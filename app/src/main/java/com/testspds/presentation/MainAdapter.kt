package com.testspds.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.testspds.R
import com.testspds.domain.Post
import kotlinx.android.synthetic.main.feature_image_view_item.view.*
import kotlin.properties.Delegates

class MainAdapter : RecyclerView.Adapter<MainAdapter.FeatureViewHolder>() {

    var posts: List<Post> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeatureViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.feature_image_view_item, parent, false)
        return FeatureViewHolder(view)
    }

    override fun getItemCount(): Int = posts.size

    override fun onBindViewHolder(holder: FeatureViewHolder, position: Int) {
        if (position != RecyclerView.NO_POSITION) {
            val post: Post = posts[position]
            holder.bind(post)
        }
    }

    fun updateData(newList: List<Post>) {
        posts = newList.filter { it.featured_image != "" }
    }

    class FeatureViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(post: Post) {
            Glide.with(itemView.context)
                .load(post.featured_image)
                .centerCrop()
                .into(itemView.featureImage)
        }
    }

}
