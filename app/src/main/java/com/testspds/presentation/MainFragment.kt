package com.testspds.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.testspds.R
import com.testspds.domain.Post
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    private val viewModel: MainViewModel by viewModel()
    private val adapter = MainAdapter()

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
        initViewModel()
    }

    private fun initViewModel() {
        viewModel.showError.observe(activity!!, Observer<String> {
            Toast.makeText(activity!!, it, Toast.LENGTH_LONG).show()
            showEmptyList(true)
        })
        viewModel.postList.observe(activity!!, Observer<List<Post>> {
            showEmptyList(it?.size == 0)
            adapter.updateData(it)
        })
        viewModel.loadInformation()
    }

    private fun setupRecycler() {
        list.adapter = adapter
        list.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        list.layoutManager = LinearLayoutManager(activity)
    }

    private fun showEmptyList(show: Boolean) {
        if (show) {
            emptyList.visibility = View.VISIBLE
            list.visibility = View.GONE
        } else {
            emptyList.visibility = View.GONE
            list.visibility = View.VISIBLE
        }
    }

    //TODO load items from https://public-api.wordpress.com/rest/v1/freshly-pressed?number=30
    // and display the "Featured Image" for each post that has one

}
