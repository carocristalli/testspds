package com.testspds.domain

class WPResponse {
    val number: Int? = null
    val posts: List<Post> = emptyList()
}
