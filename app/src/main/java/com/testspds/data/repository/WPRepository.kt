package com.testspds.data.repository

import com.testspds.data.UseCaseResult
import com.testspds.data.service.WPApi
import com.testspds.domain.Post

interface WPRepository {
    suspend fun getInformation(): UseCaseResult<List<Post>>
}

class WPRepositoryImpl(private val wpApi: WPApi) : WPRepository {
    private val RESULTS = 30

    override suspend fun getInformation(): UseCaseResult<List<Post>> {
        return try {
            val result = wpApi.searchInformationAsync(RESULTS).await()
            UseCaseResult.Success(result.posts)
        } catch (ex: Exception) {
            UseCaseResult.Error(ex)
        }
    }
}
