package com.testspds.data.service

import com.testspds.domain.WPResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface WPApi {

    @GET("/rest/v1/freshly-pressed")
    fun searchInformationAsync(
        @Query("number") number: Int
    ): Deferred<WPResponse>

}
